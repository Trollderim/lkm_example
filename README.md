# Linux Kernel Module Example
This is a simple example project to test linux kernel module development.

It is based on an online tutorial by [sourcerer](https://blog.sourcerer.io/writing-a-simple-linux-kernel-module-d9dc3762c234). Thank you for that!
